package by.sofclub.pro.service;

import by.sofclub.pro.model.Car;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService {

    private final List<Car> list = new ArrayList<>();

    @PostConstruct
    private void init() {
        /*
        альтернатива Builder
        Car.builder()
        .id()
        .name()
        .price()
        .description()
         */
        list.add(new Car(1, "Audi A4", BigDecimal.valueOf(5250.0),
                "2.0(131) / 2005г. / 176000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(2, "Audi Q5", BigDecimal.valueOf(12500.0),
                "2,0(211) / 2011г. / 66000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(3, "Opel Antara", BigDecimal.valueOf(6350.0),
                "1.7(80) / 2015г. / 52000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(4, "Hyundai Solaris", BigDecimal.valueOf(9500.0),
                "1.6(123) / 2016г. / 70000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(5, "Kia Sportage", BigDecimal.valueOf(13800.0),
                "2,0 (150) / 2013г. / 70000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(6, "LADA Kalina", BigDecimal.valueOf(3450.0),
                "84(89.1) / 2011г. / 84000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(7, "Mercedes-Benz CLS", BigDecimal.valueOf(7900.0),
                "3,5(272) / 2005г. / 180000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(8, "Porsche Cayenne", BigDecimal.valueOf(49000.0),
                "3.6(420) / 2015г. / 70000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(9, "Shkoda Rapid", BigDecimal.valueOf(9400.0),
                "1,6(90) / 2018г. / 34000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(10, "Toyota Land Cruiser", BigDecimal.valueOf(11000.0),
                "4,2(131) / 1999г. / 400000 км.", Boolean.FALSE, Boolean.FALSE));
        list.add(new Car(11, "УАЗ Патриот", BigDecimal.valueOf(8300.0),
                "2,2(113,6) / 2015г. / 88000 км.", Boolean.FALSE, Boolean.FALSE));

    }

    @Override
    public List<Car> getCars() {
        return list.stream()
                .filter(c -> !c.getIsOnBasket() && !c.getIsSale())
                .collect(Collectors.toList());
    }

    @Override
    public List<Car> getCarsInBasket() {
        return list.stream()
                .filter(Car::getIsOnBasket)
                .collect(Collectors.toList());
    }

    @Override
    public List<Car> getSaleCars() {
        return list.stream()
                .filter(Car::getIsSale)
                .collect(Collectors.toList());
    }

    private Car getCarById(Integer id) {
        Optional<Car> car = list.stream()
                .filter(c -> c.getId().equals(id))
                .findFirst();
        return car.orElse(null);
    }

    @Override
    public void addCarToBasket(Integer id) {
        Car car = getCarById(id);
        if (car != null) {
            car.setIsOnBasket(Boolean.TRUE);
        }
    }

    @Override
    public void removeCarFromBasket(Integer id) {
        Car car = getCarById(id);
        if (car != null) {
            car.setIsOnBasket(Boolean.FALSE);
        }
    }

    @Override
    public void saleCar(Integer id) {
        Car car = getCarById(id);
        if (car != null) {
            car.setIsSale(Boolean.TRUE);
        }
    }

}
