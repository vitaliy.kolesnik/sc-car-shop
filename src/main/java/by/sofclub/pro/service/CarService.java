package by.sofclub.pro.service;

import by.sofclub.pro.model.Car;

import java.util.List;

public interface CarService {
    List<Car> getCars();
    List<Car> getCarsInBasket();
    List<Car> getSaleCars();

    void addCarToBasket(Integer id);
    void removeCarFromBasket(Integer id);
    void saleCar(Integer id);
}
