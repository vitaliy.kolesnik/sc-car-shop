package by.sofclub.pro.controller;

import by.sofclub.pro.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CarController {
    @Autowired
    private CarService carService;

    @GetMapping("/cars")
    public String getList(Model model) {
        model.addAttribute("cars", carService.getCars());
        return "cars";
    }

    @GetMapping("/basket")
    public String getCarsInBasket(Model model) {
        model.addAttribute("cars", carService.getCarsInBasket());
        return "basket";
    }

    @GetMapping("/history")
    public String getSaleCars(Model model) {
        model.addAttribute("cars", carService.getSaleCars());
        return "history";
    }

    @PostMapping("/cars")
    public String addCarToBasket(@RequestParam("car_id") String car_id, Model model) {
        carService.addCarToBasket(Integer.valueOf(car_id));
        model.addAttribute("cars", carService.getCars());
        return "cars";
    }

    @PostMapping("/remove")
    public String removeCar(@RequestParam("car_id") String car_id, Model model) {
        carService.removeCarFromBasket(Integer.valueOf(car_id));
        model.addAttribute("cars", carService.getCarsInBasket());
        return "basket";
    }

    @PostMapping("/buy")
    public String buyCar(@RequestParam("car_id") String car_id, Model model) {
        carService.addCarToBasket(Integer.valueOf(car_id));
        model.addAttribute("cars", carService.getCarsInBasket());
        return "basket";
    }

}
