package by.sofclub.pro.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
//@AllArgsConstructor
@NoArgsConstructor
public class Car {
    private Integer id;
    private String name;
    private BigDecimal price;
    private String description;
    private String image;
    private Boolean isOnBasket;
    private Boolean isSale;

    public Car(Integer id, String name, BigDecimal price, String description, Boolean isOnBasket, Boolean isSale) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.image = String.format("/images/%d.png", id);
        this.isOnBasket = isOnBasket;
        this.isSale = isSale;
    }
}


